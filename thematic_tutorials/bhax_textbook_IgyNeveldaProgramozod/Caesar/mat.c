#include <stdio.h>
#include <stdlib.h>

int main ()
{
   int sorszam = 5;
    double **mat;
	printf("%p\n", &mat);

	//mem�ria vizsg�lata, double* lefoglal�sa
    if ((mat = (double **) malloc (sorszam*sizeof(double *))) == NULL)
        return -1;
		
	printf("%p\n", mat);

	//double lefoglal�sa
    for (int i = 0; i < sorszam; ++i)
        if ((mat[i] = (double *) malloc ((i+1)*sizeof(double))) == NULL)
            return -1;
			
	printf("%p\n", mat[0]);

	//szamolas 1
    for (int i = 0; i < sorszam; ++i)
        for (int j = 0; j < i + 1; ++j)
            mat[i][j] = i * (i + 1) / 2 + j;
			
	//KIIRATAS
    for (int i = 0; i < sorszam; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", mat[i][j]);
        printf ("\n");
    }
	
	//szamolas 2
    mat[3][0] = 42.0; 			//i=3, j=0
    (*(mat + 3))[1] = 43.0;		//i=3, j=1, ha nincs kulso () i=4, j=0
    *(mat[3] + 2) = 44.0;		//i=3, j=2
    *(*(mat + 3) + 3) = 45.0;	//i=3, j=3
	
	//KIIRATAS	
    for (int i = 0; i < sorszam; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", mat[i][j]);
        printf ("\n");
    }

    for (int i = 0; i < sorszam; ++i)
        free (mat[i]);

    free (mat);

    return 0;
}