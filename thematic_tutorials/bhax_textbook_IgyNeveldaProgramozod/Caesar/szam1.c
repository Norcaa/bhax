#include <stdio.h>
#include <stdlib.h>

int main ()
{	
    	int sorszam = 5;
    	double **matrix;
    	   
    	if ((matrix = (double **) malloc (sorszam * sizeof (double *))) == NULL)
    	{
        	return -1; 
    	}
    
    	for (int i = 0; i < sorszam; ++i)
    	{
        	if ((matrix[i] = (double *) malloc ((i + 1) * sizeof (double))) == NULL)
        	{
            	return -1;
        	}

    	}

    	//CIKLUSON BEL�LI CIKLUS
    
    	for (int i = 0; i < sorszam; ++i) //5x fut le
    	{
		printf("%d.futas: \n", i+1);
		printf("i=%d, ", i);
        	for (int j = 0; j < i + 1; ++j) //(i+1)x fut le
		{
	    		printf("j=%d", j);
            		matrix[i][j] = i * (i + 1) / 2 + j;
	    		printf("\nAz ertek: %g\n", matrix[i][j]); //%d nem m�k�dik!!
		}
		printf("\n");
    	}
    
    	//%f - tizedes lebeg�pontos
	
    	printf ("A MATRIX:\n");
    	for (int i = 0; i < sorszam; ++i)
    	{
        	for (int j = 0; j < i + 1; ++j)
            		printf ("%f, ", matrix[i][j]);
        	printf ("\n");
    	}

    	

	//matrix[3][0] = *(*(matrix + 3))
	//matrix[3][3] = *(*(matrix + 3)+3))

	printf ("%g, ", matrix[3][3]);
	printf ("%g\n", *(*(matrix + 3)+3));

	printf ("%g, ", matrix[3][0]);
	printf ("%g\n", *(*(matrix + 3)));


	
	for (int i = 0; i < sorszam; ++i)
        	free (matrix[i]);
    	free (matrix);
    	return 0;
}
