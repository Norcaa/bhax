#include <stdio.h>
#include <stdlib.h>

int f(int a,int b)
{
	return a+b;
}
int g(int a)
{
	return a;
}
int h(int a)
{
	return a;
}

int main()
{
	int i;
	int tomb[5]={11,12,13,14,15}; 
	
	//1.
	for(i=0; i<5; ++i)
		printf("%d ", i);
	printf("\n");
	
	//2.
	for(i=0; i<5; i++)
		printf("%d ", i);
	printf("\n");
	
	//3.
	for(i=0; i<5; tomb[i] = i++)
		printf("%d ", tomb[i]);
	printf("- HIBAS! \n");
	
	int a = 10;
	int *d = &a;
	int *s = &a;
	int n = 10;
	
	//4.	
	for(i=0; i<n && (*d++ = *s++); ++i)
		printf("%d ", i);
	printf("- HIBAS! \n");
		
	//5
	printf("%d %d", f(a, ++a), f(++a, a));
	printf(" - HIBAS! \n");
	
	//6
	printf("%d %d", g(a), a);
	printf("\n");
	
	//7
	printf("%d %d", h(&a), a);
	printf("\n");
	
	return 0;
}