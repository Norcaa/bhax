#include <iostream>
#include "png++/png.hpp"
#include <sys/times.h>
#include <complex>
using namespace std;

#define MERET 600 
#define ITER_HAT 32000

void
mandel (int kepadat[MERET][MERET]) 
{
    //IDŐ
    clock_t delta = clock ();
    struct tms tmsbuf1, tmsbuf2;
    times (&tmsbuf1);

	//SZÁMÍTÁS
    float a = -2.0, b = .7, c = -1.35, d = 1.35;
    int szelesseg = MERET, magassag = MERET, iteraciosHatar = ITER_HAT;
		
	float dx = (b - a) / szelesseg; 
    float dy = (d - c) / magassag;		
	float reC; 
	float imC;

    int iteracio = 0;
	
    // Végigmegyünk a szélesség x magasság rácson:
    for (int j = 0; j < magassag; ++j) 
    {
        //sor = j;
        for (int k = 0; k < szelesseg; ++k) 
        {
			reC = a + k * dx; 
			imC = d - j * dy; 
			complex <float> c(reC,imC);
				
			// z_0 = 0 = (reZ, imZ)
			complex <float> z(0,0);			
            iteracio = 0; //

            while (abs(z) < 4 && iteracio < iteraciosHatar) 
            {
				z = z * z + c;
                ++iteracio;
            }
            kepadat[j][k] = iteracio;
        }
    }
	
	//IDŐ
    times (&tmsbuf2);
    cout << tmsbuf2.tms_utime - tmsbuf1.tms_utime
              + tmsbuf2.tms_stime - tmsbuf1.tms_stime << endl;

    delta = clock () - delta;
    cout << (float) delta / CLOCKS_PER_SEC << " sec" << endl;

}

int
main (int argc, char *argv[]) 
{
	if (argc != 2)
    {
        cout << "Hasznalat: ./mandelpng fajlnev";
        return -1;
    }

    int kepadat[MERET][MERET];
    mandel(kepadat);

    png::image < png::rgb_pixel > kep (MERET, MERET);

    for (int j = 0; j < MERET; ++j)
    {
        //sor = j;
        for (int k = 0; k < MERET; ++k)
        {
            kep.set_pixel (k, j, png::rgb_pixel (255 - (255 * kepadat[j][k]) / ITER_HAT, 
												255 - (255 * kepadat[j][k]) / ITER_HAT, 
												255 - (255 * kepadat[j][k]) / ITER_HAT)); 
        }
    }

    kep.write (argv[1]);
    cout << argv[1] << " mentve" << endl;
}
