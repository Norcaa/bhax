public class Animal {
	public String _gender;
	public int _age;
	public double _size;
	
	public class Predator {
		public int _victim;
		public int _tooth;
		public Object _hunting__;
	}
	public class Herbivore {
		public String _plant;
		public Object _graze__;
	}
	public class Omnivore {
		public String _plant2;
		public int _victim2;
	}
}