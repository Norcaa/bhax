import java.awt.Color;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FullScreen {
   public static void main(String[] args) {
      GraphicsEnvironment graphics =
      GraphicsEnvironment.getLocalGraphicsEnvironment();
      GraphicsDevice device = graphics.getDefaultScreenDevice();
      JFrame frame = new JFrame("Fullscreen");
      JPanel panel = new JPanel();
      JLabel label = new JLabel("", JLabel.CENTER);
      panel.setBackground(Color.BLUE);
      label.setOpaque(true);
      frame.add(panel);
      panel.add(label);
      frame.setUndecorated(true);
      frame.setResizable(false);
      device.setFullScreenWindow(frame);
      //label.setText("Teljes kepernyo");
      //frame.add(label);
   }
}