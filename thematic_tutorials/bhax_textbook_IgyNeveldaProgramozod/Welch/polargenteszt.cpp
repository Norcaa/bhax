//6.1 ELSŐ OSZTÁLYOM
//FORDÍTÁS: g++ polargen.cpp polargenteszt.cpp -o polargen
//FUTTATÁS: ./polargen
//http://nyelvek.inf.elte.hu/leirasok/Delphi/index.php?chapter=9#section_2_1

#include <iostream>
#include "polargen.h"

//10 random szánot generál

int
main (int argc, char **argv)
{
  PolarGen pg; //objektum

  for (int i = 0; i < 10; ++i) //10!!
    std::cout << pg.kovetkezo () << std::endl;

  return 0;
}