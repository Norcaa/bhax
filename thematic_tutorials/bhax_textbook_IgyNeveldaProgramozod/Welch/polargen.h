#ifndef POLARGEN__H
#define POLARGEN__H

#include <cstdlib>
#include <cmath>
#include <ctime>

class PolarGen
{
public:
  PolarGen () //konstruktor
  {
    nincsTarolt = true; //logika tagot inicializás igazra
    std::srand (std::time (NULL)); //random szám generálása
  }
   ~PolarGen () //destruktor, felszabadítja az lefoglalt erőforrásokat
  {
  }
  double kovetkezo (); //ezzel a függvénnyel generáljuk a számokat

private:
  bool nincsTarolt;
  double tarolt;
};
#endif